FROM thecodingmachine/php:8.0-v4-apache

COPY ./php.ini /etc/php/8.0/cli/php.ini
COPY ./php.ini /etc/php/8.0/fpm/php.ini

COPY ./ /var/www/html/

WORKDIR /var/www/html/

ENV APACHE_DOCUMENT_ROOT=www/

RUN sudo chmod +x ./bin/console
RUN mkdir -p temp log
RUN sudo chmod -R a+rw temp log
