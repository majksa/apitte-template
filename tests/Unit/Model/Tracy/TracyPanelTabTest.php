<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Tracy;

use App\Model\Tracy\TracyPanelTab;
use Tester\Assert;
use Tests\Toolkit\BaseTestCase;

require __DIR__ . '/../../../Bootstrap.php';

/** @testCase */
final class TracyPanelTabTest extends BaseTestCase {

    public function testHtml(): void {
        $panelContent = new TracyPanelTab('name', 'title', 'image-url');
        Assert::equal(
            '<span title="title"><img src="image-url" alt="" width="15" height="15"><span class="tracy-label">name</span></span>',
            $panelContent->toHtml(),
        );
    }

}

(new TracyPanelTabTest())->run();
