<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Security;

use App\Model\Security\Passwords;
use Nette\InvalidArgumentException;
use Tester\Assert;
use Tests\Toolkit\BaseTestCase;
use ValueError;

require __DIR__ . '/../../../Bootstrap.php';

/** @testCase */
final class PasswordsTest extends BaseTestCase {

    public function testHashing(): void {
        $passwords = new Passwords();
        $hash = $passwords->hash('original');
        Assert::throws(
            static fn () => $passwords->hash(''),
            InvalidArgumentException::class,
            'Password can not be empty.',
        );
        Assert::true($passwords->verify('original', $hash));
        Assert::false($passwords->verify('n-original', $hash));
        Assert::false($passwords->needsRehash($hash));
    }

    public function testHashingError(): void {
        $passwords = new Passwords('');
        Assert::throws(static fn () => $passwords->hash('original'), ValueError::class);
    }

}

(new PasswordsTest())->run();
