<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Server;

use App\Model\Server\CookieManager;
use App\Model\Server\ICookieManager;
use Tester\Assert;
use Tests\Toolkit\BaseContainerTestCase;
use function time;

require __DIR__ . '/../../../Bootstrap.php';

/** @testCase */
final class CookieTest extends BaseContainerTestCase {

    public function testCreateAndDelete(): void {
        $cookie = $this->container->getByType(ICookieManager::class)->new('test');
        $cookie->value = 'xyz';
        Assert::null($cookie->get());
        Assert::true($cookie->save());
        Assert::equal('xyz', $cookie->get());
        Assert::true($cookie->delete());
        Assert::null($cookie->get());
    }

    public function testTime(): void {
        $cookie = $this->container->getByType(ICookieManager::class)->new('test');
        $cookie->value = 'xyz';
        $cookie->save();
        Assert::equal('xyz', $cookie->get());
        $cookie->time = time() - 3_600;
        $cookie->save();
        Assert::null($cookie->get());
        $cookie->time = time() + 3_600;
        $cookie->save();
        Assert::equal('xyz', $cookie->get());
    }

    public function testFixCoverageManualTested(): void {
        $manager = new CookieManager();
        $cookie = $manager->new('test');
        $manager->save($cookie);
        Assert::null($manager->get('test'));
    }

}

(new CookieTest())->run();
