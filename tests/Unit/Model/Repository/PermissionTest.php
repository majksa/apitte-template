<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Repository;

use App\Model\Database\EntityManager;
use Tester\Assert;
use Tests\Toolkit\BaseContainerTestCase;
use Tests\Toolkit\InMemoryEntityManager;

require __DIR__ . '/../../../Bootstrap.php';

/** @testCase */
final class PermissionTest extends BaseContainerTestCase {

    private EntityManager $em;

    protected function setUp(): void {
        parent::setUp();

        $this->em = $this->getService(InMemoryEntityManager::class);
    }

    public function testFindOneByName(): void {
        Assert::count(0, $this->em->getUserRepository()->findAll());
    }

}

(new PermissionTest())->run();
