<?php declare(strict_types = 1);

namespace Tests\Toolkit;

use App\Model\Database\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\Persistence\ObjectRepository;
use Mockery;
use ReflectionClass;
use ReflectionException;
use function assert;
use function count;
use function ucfirst;

class InMemoryEntityManager extends EntityManager {

    protected ArrayCollection $database;

    protected ArrayCollection $repositories;

    public function __construct() {
        $this->database = new ArrayCollection();
        $this->repositories = new ArrayCollection();
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null): mixed {
        return $this->getTableData($className)->filter(
            fn ($entity) => $this->checkDatabaseQuery($entity, ['id' => $id]),
        )->first();
    }

    public function persist($object): void {
        $this->getTableData($object::class)->add($object);
    }

    /**
     * @param class-string $className
     */
    private function getTableData(string $className): ArrayCollection {
        if (!$this->database->containsKey($className)) {
            $this->database[$className] = new ArrayCollection();
        }

        return $this->database[$className];
    }

    public function flush($entity = null): void {
        // DO NOTHING
    }

    public function getRepository($className): ObjectRepository {
        if (!$this->repositories->containsKey($className)) {
            try {
                $reflection = new ReflectionClass($className);
                $attributes = $reflection->getAttributes(Entity::class);
                if (count($attributes) > 0) {
                    $repositoryClass = $attributes[0]->newInstance()->repositoryClass ?? EntityRepository::class;
                    $repo = Mockery::mock($repositoryClass, [$this, new ClassMetadata($className)])->makePartial();
                    $this->repositories[$className] = $repo;
                    $repo->shouldReceive('findBy')->andReturnUsing(
                        fn ($query) => $this->findAll($className, $query)->toArray(),
                    );
                    $repo->shouldReceive('findOneBy')->andReturnUsing(
                        fn ($query) => $this->findAll($className, $query)->first(),
                    );
                    $repo->shouldReceive('count')->andReturnUsing(
                        fn ($query) => $this->findAll($className, $query)->count(),
                    );
                    foreach ($reflection->getProperties() as $property) {
                        if (count($property->getAttributes(Column::class)) === 0) {
                            continue;
                        }

                        $field = $property->getName();
                        $suffix = ucfirst($property->getName());
                        $repo->shouldReceive("findBy$suffix")->andReturnUsing(
                            fn ($argument) => $this->findAll($className, [$field => $argument])->toArray(),
                        );
                        $repo->shouldReceive("findOneBy$suffix")->andReturnUsing(
                            fn ($argument) => $this->findAll($className, [$field => $argument])->first(),
                        );
                        $repo->shouldReceive("countBy$suffix")->andReturnUsing(
                            fn ($argument) => $this->findAll($className, [$field => $argument])->count(),
                        );
                    }

                    assert($repo instanceof EntityRepository);

                    return $repo;
                }
            } catch (ReflectionException) {
            }
        }

        return $this->repositories[$className];
    }

    /**
     * @param class-string<T> $entityClass
     * @param array<string,mixed>  $query
     * @return ArrayCollection<T>
     *
     * @template T
     */
    private function findAll(string $entityClass, array $query): ArrayCollection {
        return $this->getTableData($entityClass)->filter(
            static fn ($entity) => self::checkDatabaseQuery($entity, $query),
        );
    }

    /**
     * @param array<string,mixed>  $query
     */
    private static function checkDatabaseQuery(object $entity, array $query): bool {
        foreach ($query as $key => $value) {
            if ($entity->{$key} !== $value) {
                return false;
            }
        }

        return true;
    }

}
