<?php declare(strict_types = 1);

use App\Model\Server\FileManager;
use Maxa\Ondrej\Nette\GraphQL\Application\Application;
use Nette\InvalidStateException;

require __DIR__ . '/../vendor/autoload.php';

// Let bootstrap create Dependency Injection container.
$configurator = App\Bootstrap::boot();
$container = $configurator->createContainer();

$uri = filter_input(INPUT_SERVER, 'REQUEST_URI');

if (str_starts_with($uri, '/graphql')) {
    if ((filter_input(INPUT_GET, 'query') ?? file_get_contents('php://input') ?? '') !== '') {
        $container->getByType(Application::class)->run();
    }
    exit();
} elseif (str_starts_with($uri, '/upload')) {
    header('Content-Type: application/json');
    if ($uri !== '/upload') {
        echo '{"error":"File not found"}';
        http_response_code(404);
        exit();
    }

    $manager = $container->getByType(FileManager::class);
    $security = $container->getByType(\App\Model\Security\Security::class);

    $token = filter_input(INPUT_POST, 'token');

    try {
        if ($token === null) {
            throw new InvalidStateException('Token is empty!', 403);
        }

        $tokenEntity = $security->validateUploadToken($token);
    } catch (InvalidStateException $e) {
        echo "{\"error\":\"{$e->getMessage()}\"}";
        http_response_code($e->getCode());
        exit();
    }

    $file = @$_FILES['file'];
    if ($file === null) {
        echo '{"error":"Please attach a file to be uploaded"}';
        http_response_code(400);
        exit();
    }

    try {
        $manager->save($file['tmp_name'], $file['size'], $tokenEntity);
        echo "{\"id\":\"$tokenEntity->id\", \"link\":\"{$tokenEntity->getLink()}\"}";
        http_response_code(201);
    } catch (InvalidArgumentException $e) {
        echo "{\"error\":\"{$e->getMessage()}\"}";
        http_response_code(400);
    }
}
