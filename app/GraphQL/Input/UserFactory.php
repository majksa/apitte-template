<?php declare(strict_types = 1);

namespace App\GraphQL\Input;

use App\Model\Database\EntityManager;
use App\Model\Entity\Security\User;
use App\Model\Repository\Security\User as UserRepository;
use JetBrains\PhpStorm\Pure;
use Maxa\Ondrej\Nette\DI\Service;
use TheCodingMachine\GraphQLite\Annotations\Factory;
use TheCodingMachine\GraphQLite\Exceptions\GraphQLException;

#[Service]
final class UserFactory {

    public const CREATE = 'CreateUserInput';

    public const CREATE_NO_PASS = 'CreateUserWithoutPasswordInput';

    public const BY_USERNAME = 'FindUserByUsernameInput';

    private UserRepository $users;

    public function __construct(EntityManager $em) {
        $this->users = $em->getUserRepository();
    }

    #[Pure]
    #[Factory(name: self::CREATE, default: true)]
    public function createUser(
        string $name,
        string $surname,
        string $email,
        string $phone,
        string $username,
        string $password,
    ): User {
        return new User($name, $surname, $email, $phone, $username, $password);
    }

    #[Pure]
    #[Factory(name: self::CREATE_NO_PASS, default: false)]
    public function createUserWithoutPassword(
        string $name,
        string $surname,
        string $email,
        string $phone,
        string $username,
    ): User {
        return new User($name, $surname, $email, $phone, $username, null);
    }

    /**
     * @throws GraphQLException
     */
    #[Factory(name: self::BY_USERNAME, default: false)]
    public function findUserByUsername(string $username): User {
        $user = $this->users->findOneByUsername($username);
        if ($user === null) {
            throw new GraphQLException(
                "User with the specified username: $username does not exist.",
                category: 'Not Found',
                extensions: [
                    'username' => $username,
                ],
            );
        }

        return $user;
    }

}
