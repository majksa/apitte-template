<?php declare(strict_types = 1);

namespace App\GraphQL;

interface Rights {

    public const CREATE_REGISTRATION = 'CREATE_REGISTRATION';

}
