<?php declare(strict_types = 1);

namespace App\GraphQL\Controller;

use App\GraphQL\Input\UserFactory;
use App\Model\Database\EntityManager;
use App\Model\Entity\Security\Fido;
use App\Model\Entity\Security\User;
use App\Model\Security\EmailAlreadyInUseException;
use App\Model\Security\FidoRegistration;
use App\Model\Security\Security;
use App\Model\Security\UsernameTakenException;
use CodeLts\U2F\U2FServer\U2FException;
use CodeLts\U2F\U2FServer\U2FServer;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Exceptions\GraphQLException;
use TheCodingMachine\GraphQLite\Validator\ValidationFailedException;
use function assert;

final class FidoController {

    public string $appId = 'graphql.id';

    private SessionSection $section;

    public function __construct(
        private Security $security,
        private ValidatorInterface $validator,
        private EntityManager $em,
        Session $session,
    ) {
        $this->section = $session->getSection('fido');
    }

    /**
     * @throws GraphQLException
     */
    #[Mutation(name: 'createFido')]
    public function create(): FidoRegistration {
        try {
            $registration = U2FServer::makeRegistration($this->appId);
            $this->section->set('registration', $registration['request']);

            return new FidoRegistration(
                request: Json::encode($registration['request']),
                signatures: Json::encode($registration['signatures']),
            );
        } catch (U2FException | JsonException $e) {
            throw new GraphQLException($e->getMessage(), previous: $e);
        }
    }

    /**
     * @throws GraphQLException
     */
    #[Mutation(name: 'registerFido')]
    public function add(#[UseInputType(UserFactory::CREATE_NO_PASS)] ?User $user, string $registration): User {
        if ($user === null) {
            if (($user = $this->security->getLogged()) === null) {
                throw new GraphQLException('You need to be logged in', 401, category: 'Unauthorized');
            }
        } else {
            ValidationFailedException::throwException($this->validator->validate($user));
            try {
                $this->security->register($user);
            } catch (EmailAlreadyInUseException | UsernameTakenException $e) {
                throw new GraphQLException($e->getMessage(), 409, $e, 'Conflict');
            }
        }

        try {
            $user->addFido(U2FServer::register(
                $this->section->get('registration'),
                Json::decode($registration),
            ));

            return $user;
        } catch (U2FException | JsonException $e) {
            throw new GraphQLException($e->getMessage(), previous: $e);
        }
    }

    /**
     * @throws GraphQLException
     */
    #[Mutation(name: 'fidoAuthStart')]
    public function login(#[UseInputType(UserFactory::BY_USERNAME)] User $user): string {
        try {
            /** @phpstan-ignore-next-line */
            $authentication = U2FServer::makeAuthentication($user->getFidoHandles(), $this->appId);
            $this->section->set('authorization', $authentication);

            return Json::encode($authentication);
        } catch (JsonException $e) {
            throw new GraphQLException($e->getMessage(), previous: $e);
        }
    }

    /**
     * @throws GraphQLException
     */
    #[Mutation(name: 'fidoAuthFinish')]
    public function loginFinish(#[UseInputType(UserFactory::BY_USERNAME)] User $user, string $authentication): User {
        try {
            $response = U2FServer::authenticate(
                $this->section->get('authorization'),
                /** @phpstan-ignore-next-line */
                $user->getFidoHandles(),
                Json::decode($authentication),
            );

            $fido = $this->em->find(Fido::class, $response->id);
            assert($fido instanceof Fido);
            $fido->counter = (int) $response->counter;
            $this->em->flush($fido);
            $this->security->login($user);

            return $user;
        } catch (U2FException | JsonException $e) {
            throw new GraphQLException($e->getMessage(), previous: $e);
        }
    }

}
