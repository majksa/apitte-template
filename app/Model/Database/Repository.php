<?php declare(strict_types = 1);

namespace App\Model\Database;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

/**
 * @template TEntityClass of Entity
 * @extends EntityRepository<TEntityClass>
 */
abstract class Repository extends EntityRepository {

    public function getEm(): EntityManagerInterface {
        return $this->_em;
    }

}
