<?php declare(strict_types = 1);

namespace App\Model\Database\Traits;

use App\Model\Utils\Server;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;

trait TOnUpdate {

    #[Field]
    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    public ?DateTimeImmutable $updatedAt = null;

    #[Field]
    #[ORM\Column(type: Types::STRING, nullable: true)]
    public ?string $updatedBy = null;

    /**
     * Doctrine annotation
     *
     * @internal
     */
    #[ORM\PreUpdate]
    final public function setUpdatedAt(): void {
        $this->updatedAt = new DateTimeImmutable();
        $this->updatedBy = Server::getClientIp();
    }

}
