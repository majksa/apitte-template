<?php declare(strict_types = 1);

namespace App\Model\Repository\Security;

use App\Model\Database\Repository;
use App\Model\Entity\Security\UploadToken as UploadTokenEntity;

/**
 * @extends Repository<UploadTokenEntity>
 * @method UploadTokenEntity|null findOneByToken(string $token)
 */
final class UploadToken extends Repository {

    public function isTokenTaken(string $token): bool {
        return $this->count(['token' => $token]) > 0;
    }

}
