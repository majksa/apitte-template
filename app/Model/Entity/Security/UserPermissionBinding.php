<?php declare(strict_types = 1);

namespace App\Model\Entity\Security;

use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @extends PermissionBinding<User>
 */
#[Type]
#[ORM\Table]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class UserPermissionBinding extends PermissionBinding {

    #[Pure]
    public function __construct(
        #[Field]
        #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'bindings')]
        #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
        public User $user,
        Permission $permission,
        int $value = self::DEFAULT,
    ) {
        parent::__construct($permission, $value);
    }

    #[Pure]
    public function getHolder(): User {
        return $this->user;
    }

}
