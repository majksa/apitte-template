<?php declare(strict_types = 1);

namespace App\Model\Entity\Security;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @extends PermissionHolder<RolePermissionBinding>
 */
#[Type]
#[ORM\Table(name: 'role')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Role extends PermissionHolder {

    /** @var Collection<int,RolePermissionBinding> */
    #[ORM\OneToMany(mappedBy: 'role', targetEntity: RolePermissionBinding::class)]
    public Collection $bindings;

    /** @var Collection<int,User> */
    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'roles')]
    public Collection $users;

    #[Pure]
    public function __construct(
        #[Field]
        #[ORM\Column(type: Types::STRING, length: 255)]
        public string $name,
    ) {
        $this->users = new ArrayCollection();
        $this->bindings = new ArrayCollection();
    }

    /**
     * @return Collection<int,RolePermissionBinding>
     */
    #[Pure]
    public function getBindings(): Collection {
        return $this->bindings;
    }

    public function create(Permission $permission): RolePermissionBinding {
        return $this->bindings[] = new RolePermissionBinding($this, $permission);
    }

    public function addUser(User $user): void {
        $this->users[] = $user;
    }

    public function removeUser(User $user): void {
        $this->users->removeElement($user);
    }

}
